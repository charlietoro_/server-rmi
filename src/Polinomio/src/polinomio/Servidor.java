/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polinomio;

import java.rmi.Naming;

/**
 *
 * @author charlie
 */
public class Servidor {
    public Servidor() {
        try {
            System.setProperty("java.rmi.server.codebase", "file:/home/admin/NetBeansProjects/RMI/src");
            Interface i = (Interface) new Implementacion();
            Naming.rebind("rmi://192.168.1.104/Polinomio", i);
        } catch (Exception ex) {
            System.out.println("Error: " + ex);
        }
    }
    
    public static void main( String[] args) {
        new Servidor();
        System.out.println( "Servidor en espera" );
    }
}
